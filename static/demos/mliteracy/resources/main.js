var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Noob Node Graphic Interface
 *
 * @version 2.1.1
 * @see <url>
 * @dependence: jQuery
 *
 * Simple GI
 *
 * @
 * import * as NNGI from "./NNGI"
 *
 * interface App {
 * }
 *
 * let widget: NNGI.Main<App> = new NNGI.Main()
 * widget.children.push(new NNGI.Tag('div', {text: "Hello World!"}))
 *
 * let app: App = {}
 *
 * $(document).ready(function() {
 *     widget.build(app, undefined, $('body'))
 *
 *     $(window).resize( () => widget.eventWidgetUp(new NNGI.Resize) )
 * });
 * @
 */
define("NNGI", ["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    /**
     * Foundation for custom widget
     */
    var Widget = /** @class */ (function () {
        function Widget() {
            this.children = [];
            this.buildStatus = false;
        }
        Widget.prototype.build = function (state, parent, target) {
            var result = false;
            if (!this.buildStatus) {
                this.parent = parent;
                this.state = state;
                var response = this.draw(target);
                this.it = response.it;
                this.container = response.container;
                result = true;
            }
            for (var i = 0; i < this.children.length; i++) {
                if (this.children[i].build(state, this, this.container))
                    result = true;
            }
            if (result) {
                this.afterDraw();
                this.buildStatus = true;
            }
            return result;
        };
        Widget.prototype.buildChildren = function () {
            if (this.state && this.container) {
                for (var i = 0; i < this.children.length; i++) {
                    this.children[i].build(this.state, this, this.container);
                }
            }
        };
        Widget.prototype.afterDraw = function () { };
        Widget.prototype.event = function (event) { return event; };
        /**
         * Add child in this node and call build(...) for it
         */
        Widget.prototype.append = function (widget) {
            this.children.push(widget);
            widget.build(this.state, this, this.container);
        };
        Widget.prototype.empty = function () {
            for (var i = 0; i < this.children.length; i++) {
                this.children[i].remove();
            }
            this.children = [];
        };
        Widget.prototype.remove = function () {
            this.it.remove();
            this.children = [];
        };
        /**
         * If this is root of tree to remove html, but not remove this node
         */
        Widget.prototype.removeExtra = function () {
            this.remove();
            if (this.parent !== undefined) {
                this.parent.reindex();
                this.parent.removeChildrenByIndex(this.__index);
            }
        };
        Widget.prototype.reindex = function () {
            for (var i = 0; i < this.children.length; i++) {
                this.children[i].__index = i;
            }
        };
        Widget.prototype.removeChildrenByIndex = function (n) {
            var _this = this;
            this.children = this.children.filter((function (x) { return x.__index != _this.__index; }), this);
        };
        /**
         * Create event in widget and children
         */
        Widget.prototype.eventWidgetUp = function (event) {
            var response;
            response = this.event(event);
            var i = 0;
            while (response.stop !== true && i < this.children.length) {
                response = this.children[i].eventWidgetUp(response);
                i++;
            }
            return response;
        };
        /**
         * Create event in widget and parents
         */
        Widget.prototype.eventWidgetDown = function (event) {
            var response;
            response = this.event(event);
            var t = this;
            while (response.stop !== true && t.parent !== undefined) {
                t = t.parent;
                response = t.event(response);
            }
            return response;
        };
        Widget.prototype.findRoot = function () {
            var t = this;
            while (t.parent !== undefined) {
                t = t.parent;
            }
            return t;
        };
        Widget.prototype.findParents = function () {
            var result = [];
            var t = this;
            while (t.parent !== undefined) {
                t = t.parent;
                result.push(t);
            }
            return result;
        };
        Widget.prototype.findWidgetUp = function (fn) {
            var result = false;
            if (fn(this)) {
                return this;
            }
            else {
                for (var i = 0; i < this.children.length; i++) {
                    result = this.children[i].findWidgetUp(fn);
                    if (result !== false)
                        break;
                }
            }
            return result;
        };
        Widget.prototype.findWidgetUpAll = function (fn) {
            var result = [];
            if (fn(this))
                result.push(this);
            for (var i = 0; i < this.children.length; i++) {
                result.concat(this.children[i].findWidgetUpAll(fn));
            }
            return result;
        };
        Widget.prototype.findWidgetDown = function (fn) {
            var result = false;
            if (fn(this)) {
                return this;
            }
            else {
                var t = this;
                while (t.parent !== undefined) {
                    if (fn(t.parent))
                        return t.parent;
                    t = t.parent;
                }
            }
            return result;
        };
        Widget.prototype.findWidgetDownAll = function (fn) {
            var result = [];
            if (fn(this))
                result.push(this);
            if (this.parent !== undefined) {
                result.concat(this.parent.findWidgetDownAll(fn));
            }
            return result;
        };
        return Widget;
    }());
    exports.Widget = Widget;
    /**
     * Foundation for event in widget
     */
    var EventWidget = /** @class */ (function () {
        function EventWidget() {
            this.stop = false;
        }
        return EventWidget;
    }());
    exports.EventWidget = EventWidget;
    var Resize = /** @class */ (function (_super) {
        __extends(Resize, _super);
        function Resize() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return Resize;
    }(EventWidget));
    exports.Resize = Resize;
    /**
     * Empty Widget for root tree
     */
    var Main = /** @class */ (function (_super) {
        __extends(Main, _super);
        function Main() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Main.prototype.draw = function (target) {
            return { it: target, container: target };
        };
        return Main;
    }(Widget));
    exports.Main = Main;
    /**
     * Simple widget
     */
    var Tag = /** @class */ (function (_super) {
        __extends(Tag, _super);
        function Tag(tag, options) {
            if (tag === void 0) { tag = "div"; }
            if (options === void 0) { options = {}; }
            var _this = _super.call(this) || this;
            _this.tag = tag;
            _this.options = options;
            return _this;
        }
        Tag.prototype.draw = function (target) {
            var element = $("<" + this.tag + ">", this.options);
            target.append(element);
            return { it: element, container: element };
        };
        return Tag;
    }(Widget));
    exports.Tag = Tag;
});
define("App", ["require", "exports", "NNGI"], function (require, exports, NNGI) {
    "use strict";
    exports.__esModule = true;
    var list_notes = [
        { name: 'До', letter: 'C' },
        { name: 'Ре', letter: 'D' },
        { name: 'Ми', letter: 'E' },
        { name: 'Фа', letter: 'F' },
        { name: 'Соль', letter: 'G' },
        { name: 'Ля', letter: 'A' },
        { name: 'Си', letter: 'B' } // 6
    ];
    var list_octaves = [
        { name: 'Контр', letter: 'C' },
        { name: 'Большая', letter: 'G' },
        { name: 'Малая', letter: 'S' },
        { name: 'Первая', letter: '1' },
        { name: 'Вторая', letter: '2' },
        { name: 'Третья', letter: '3' } // 5
    ];
    var soundError = new Howl({
        src: ['resources/error.mp3'],
        volume: 1
    });
    var sound = new Howl({
        src: ['resources/notes.mp3'],
        volume: 1,
        sprite: listNoteSound()
    });
    function listNoteSound() {
        var list = {};
        var t = 0;
        for (var n = 0; n < list_octaves.length; n++) {
            for (var m = 0; m < list_notes.length; m++) {
                list[list_notes[m].letter + list_octaves[n].letter] = [t, 1500];
                t = t + 1500;
            }
        }
        return list;
    }
    var App = /** @class */ (function () {
        function App() {
            this.all = 0;
            this.correct = 0;
            this.next_step = false;
            this.sound = sound;
            this.soundError = soundError;
            this.play = false;
            this.timeId = 0;
            this.help = false;
        }
        App.prototype.start = function () {
            this.next_step = false; // флаг состояния смены ноты
            if (this.play)
                clearTimeout(this.timeId); // сбросить автоматическую смену ноты
            this.note = this.newNote();
            this.noteView.renderNote(this.note);
            this.control.resetMessage();
            this.control.inputReset();
        };
        App.prototype.newNote = function () {
            //return {key:0, note:0, octave:0, name:'!!!', letter: 'C1'}
            var note = {
                key: Math.floor(Math.random() * 2),
                note: Math.floor(Math.random() * 6),
                octave: Math.floor(Math.random() * 4),
                name: '', letter: ''
            };
            var index_octave = note.octave;
            if (note.key == 0)
                index_octave = note.octave + 2;
            note.name = list_notes[note.note].name + ' ' + list_octaves[index_octave].name;
            note.letter = list_notes[note.note].letter + list_octaves[index_octave].letter;
            return note;
        };
        return App;
    }());
    exports.App = App;
    var Main = /** @class */ (function (_super) {
        __extends(Main, _super);
        function Main() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.body = undefined;
            return _this;
        }
        Main.prototype.draw = function (target) {
            this.body = $('body');
            this.resize();
            var element = $('<div>', { id: 'main' });
            target.on('click', function () { $('#in').focus(); });
            target.append(element);
            return { it: element, container: element };
        };
        Main.prototype.resize = function () {
            this.body.height($(window).height());
        };
        Main.prototype.event = function (event) {
            if (event instanceof NNGI.Resize) {
                this.resize();
            }
            return event;
        };
        return Main;
    }(NNGI.Widget));
    exports.Main = Main;
    var NoteLines = /** @class */ (function (_super) {
        __extends(NoteLines, _super);
        function NoteLines() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.pointer_note = '1';
            return _this;
        }
        NoteLines.prototype.draw = function (target) {
            this.state.noteView = this;
            var element = $('<div>', { id: 'note_lines', append: [
                    $('<div>', { id: 'background_help' }),
                    $('<div>', { id: 'note1', "class": 'note_box', append: [
                            $('<div>', { "class": 'lines', append: $('<div>', { "class": 'lines_fon' }) }),
                            $('<div>', { "class": 'note' })
                        ] }),
                    $('<div>', { id: 'note2', "class": 'note_box', append: [
                            $('<div>', { "class": 'lines', append: $('<div>', { "class": 'lines_fon' }) }),
                            $('<div>', { "class": 'note' })
                        ] })
                ] });
            var help = $('<div>', { id: 'help_note', "class": 'off', append: [
                    $('<div>', { id: 'octave' })
                ] });
            var box = $('<div>', { id: 'note_lines_box', append: [element, help] });
            target.append(box);
            return { it: element, container: element };
        };
        NoteLines.prototype.renderHelp = function () {
            if (this.state.help) {
                $('#help_note').css('display', 'block');
                $('#background_help').css('display', 'block');
                $('#note_lines .lines_fon').addClass('help');
                if (!this.state.next_step)
                    this.state.control.setMessage(this.state.note.name, 2);
            }
            else {
                $('#help_note').css('display', 'none');
                $('#background_help').css('display', 'none');
                $('#note_lines .lines_fon').removeClass('help');
                if (!this.state.next_step)
                    this.state.control.setMessage('', 2);
            }
        };
        NoteLines.prototype.renderNote = function (note) {
            var zero = 0;
            var more_lines_begin = 0;
            var more_lines_height = 0;
            if (note.key > 0) {
                zero = 676; // басовый ключ
                more_lines_begin = 495;
            }
            else {
                zero = 364; // скрипичный ключ
                more_lines_begin = 209;
            }
            var top = zero - note.note * 13 - note.octave * 91;
            more_lines_height = top - more_lines_begin;
            if (more_lines_height >= 0) {
                more_lines_height = more_lines_height + 13 + 3;
            }
            else {
                more_lines_begin = more_lines_begin + more_lines_height + 13;
                more_lines_height = -1 * more_lines_height - 13;
            }
            // указатель но новую ноту
            var new_target;
            if (this.pointer_note == '1')
                new_target = '2';
            else
                new_target = '1';
            // конфигурация новой ноты
            $('#note' + new_target + ' .note').css({ top: top }); // поставить на нужную линейку
            $('#note' + new_target + ' .lines').css({
                top: more_lines_begin,
                height: more_lines_height
            }).children().first().css({ top: -1 * more_lines_begin });
            // позиция подсказки
            var octave = zero - 104 + 1 + 26 - note.octave * 91;
            // анимация смены нот
            $('#note' + this.pointer_note).animate({ left: -72 }, 80, function () { $(this).css({ left: 387 + 'px' }); }); // анимация удаления ноты
            $('#note' + new_target).animate({ left: 213 }, 80); // анимация появления ноты
            if (this.state.help)
                $('#octave').animate({ top: octave }, 80);
            else
                $('#octave').css({ top: octave + 'px' });
            // переставить указатель на текущую ноту
            this.pointer_note = new_target;
        };
        return NoteLines;
    }(NNGI.Widget));
    exports.NoteLines = NoteLines;
    var ControlElements = /** @class */ (function (_super) {
        __extends(ControlElements, _super);
        function ControlElements() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        ControlElements.prototype.draw = function (target) {
            this.state.control = this;
            var element = $('<div>', { id: 'control_elements', append: [
                    listNotes(),
                    listOctave(),
                    showResponse(),
                    this.showInput(),
                    showMessage(),
                    this.showNext(),
                    $('<div>', { id: 'options', append: [
                            this.showPlayOption(),
                            this.showHelpOption()
                        ] })
                ] });
            target.append(element);
            return { it: element, container: element };
        };
        ControlElements.prototype.resetMessage = function () {
            if (this.state.help)
                this.setMessage(this.state.note.name, 2);
            else
                this.setMessage('', 0);
        };
        ControlElements.prototype.updateScore = function () {
            $('#response .all').text(this.state.all);
            $('#response .correct').text(this.state.correct);
        };
        // type: 0 - ok, 1 - error, 2 - help
        ControlElements.prototype.setMessage = function (text, type) {
            $('#message').text(text);
            $('#message').attr('class', '');
            if (type == 0)
                $('#message').addClass('ok');
            else if (type == 1)
                $('#message').addClass('error');
            else
                $('#message').addClass('help');
        };
        ControlElements.prototype.inputReset = function () {
            $('#in').val('').removeAttr('disabled').focus();
        };
        ControlElements.prototype.inputOff = function () {
            $('#in').attr('disabled', 'disabled');
        };
        ControlElements.prototype.next = function () {
            this.inputOff();
            this.updateScore();
            this.state.next_step = true;
            if (this.state.play)
                this.state.timeId =
                    setTimeout(function () { this.state.start(); }.bind(this), 1000);
        };
        ControlElements.prototype.showInput = function () {
            var element = $('<input>', { type: 'text', id: 'in', "class": 'off', disabled: 'disabled' });
            element.on('input', function (e) {
                var t = $(e.target).val();
                t = t.toUpperCase();
                t = t.replace(' ', '');
                $('#in').val(t);
                var n = t.length;
                if (t == this.state.note.letter) {
                    this.state.all++;
                    this.state.correct++;
                    this.setMessage(this.state.note.name, 0);
                    this.state.sound.play(this.state.note.letter);
                    this.next();
                }
                else if (this.state.note.letter.substr(0, n) !== t) {
                    this.state.all++; // error
                    this.setMessage(this.state.note.name, 1);
                    this.state.soundError.play();
                    this.next();
                }
            }.bind(this));
            return $('<div>', { id: 'input', append: element });
        };
        ControlElements.prototype.showNext = function () {
            var element = $('<div>', { id: 'next', append: [
                    $('<div>', { "class": 'button' }).html('Дальше (<span>Space</span>)')
                ] });
            element.on('click', function () {
                if (this.state.next_step) {
                    this.state.start();
                }
            }.bind(this));
            return element;
        };
        ControlElements.prototype.showPlayOption = function () {
            var element = $('<div>', { id: 'play_option', "class": 'item' }).html('<span class="text">Автоматом следующую: <span class="flag">Off</span></span>');
            element.on('click', function (e) {
                if (this.state.play) {
                    this.state.play = false;
                    clearTimeout(this.state.timeId);
                    $('.flag', e.target).text('Off');
                }
                else {
                    this.state.play = true;
                    $('.flag', e.target).text('On');
                    if (this.state.next_step)
                        this.state.start();
                }
            }.bind(this));
            return element;
        };
        ControlElements.prototype.showHelpOption = function () {
            var element = $('<div>', { id: 'show_help', "class": 'item' }).html('<span class="text">Показать помощь: <span class="flag">Off</span></span>');
            element.on('click', function (e) {
                if (this.state.help) {
                    this.state.help = false;
                    $('.flag', e.target).text('Off');
                }
                else {
                    this.state.help = true;
                    $('.flag', e.target).text('On');
                }
                this.state.noteView.renderHelp();
            }.bind(this));
            return element;
        };
        return ControlElements;
    }(NNGI.Widget));
    exports.ControlElements = ControlElements;
    function showResponse() {
        var element = $('<div>', { id: 'response' }).html('Ответы: <span class="correct">0</span> из <span class="all">0</span>');
        return element;
    }
    function showMessage() {
        var element = $('<div>', { id: 'message', text: '' });
        return element;
    }
    function listNotes() {
        var ul = $('<ul>', { id: 'notes' }).html('<li class="title">Ноты:</li>' +
            '<li><span>C</span> - До</li>' +
            '<li><span>D</span> - Ре</li>' +
            '<li><span>E</span> - Ми</li>' +
            '<li><span>F</span> - Фа</li>' +
            '<li><span>G</span> - Соль</li>' +
            '<li><span>A</span> - Ля</li>' +
            '<li><span>B</span> - Си</li>');
        return ul;
    }
    function listOctave() {
        var ul = $('<ul>', { id: 'octaves' }).html('<li class="title">Октавы:</li>' +
            '<li><span>C</span>ontra - Контр</li>' +
            '<li><span>G</span>reat - Большая</li>' +
            '<li><span>S</span>mal - Малая</li>' +
            '<li><span>1</span> Line - Первая</li>' +
            '<li><span>2</span> Line - Вторая</li>' +
            '<li><span>3</span> Line - Третья</li>');
        return ul;
    }
});
///<reference path="../node_modules/@types/jquery/index.d.ts" />
/// <reference path="./howler.d.ts" />
define("main", ["require", "exports", "NNGI", "App"], function (require, exports, NNGI, App) {
    "use strict";
    exports.__esModule = true;
    var widget = new App.Main;
    widget.children.push(new App.NoteLines);
    widget.children.push(new App.ControlElements);
    var app = new App.App;
    $(document).ready(function () {
        widget.build(app, undefined, $('body'));
        $(window).resize(function () { return widget.eventWidgetUp(new NNGI.Resize); });
        $(document).keydown(function (e) {
            if (e.which == 32 && app.next_step) {
                app.start();
            }
        });
        app.start();
    });
});
//# sourceMappingURL=main.js.map