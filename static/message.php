<?php

$config = include(__DIR__.'/include/config.php');
include(__DIR__.'/include/functions.php');

/**
 * Verification
 */

$data = [
    'name'    => trim((string)@$_POST['name']),
    'email'   => trim((string)@$_POST['email']),
    'message' => trim((string)@$_POST['message']),
    'code'    => trim((string)@$_POST['code']),
];

$message = verification($data, $config);
$status = (count($message) > 0) ? 1 : 0;

/**
 * Send email
 */

if(!$status) {
    $response = sendEmail($data, $config);
    if(count($response)) {
        $status = 1;
        $message = $response;
    } else {
        $message[] = 'Сообщение отправлено.';
    }
}

/**
 * Response
 */

header('Content-Type: application/json');
echo json_encode([
    'status'  => $status,
    'message' => $message
    ]);
