<?php

$config = include(__DIR__.'/include/config.php');
$articles = include(__DIR__.'/../articles/config.php');
include(__DIR__.'/include/functions.php');

$content = '';

if(!array_key_exists('id', $_GET)) {
    // list articles
    $content = articlesHtml(array_reverse($articles));
} else {
    $id = (int)@$_GET['id'];

    $article = findArticleById($id, $articles);

    if($article !== false) {
        // show article
        $content = titleHtml($article['title'])."\n"
            .loadArticleByName($article, $config);
    } else {
        // 404 error
        header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
        $content = titleHtml('Ошибка 404').'<div>Страница не найдена</div>';
    }
}

echo loadHeadHtml([
    '<link rel="stylesheet" type="text/css" href="assets/lib/highlight/vs.css" />',
    '<script type="text/javascript" src="assets/lib/highlight/highlight.pack.js"></script>',
]);
?>
    <body>
        <div id="body"></div>
        <div id="articles-block"><?php echo $content; ?></div>
        <script type="text/javascript">
            var code = "<?php echo $config['code']; ?>"
            var mode = 1
        </script>
        <script type="text/javascript" src="main.js"></script>
        <noscript>Enable javascript execution!</noscript>
    </body>
</html>
