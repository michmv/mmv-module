<?php
$config = include(__DIR__.'/include/config.php');
include(__DIR__.'/include/functions.php');

echo loadHeadHtml();
?>
    <body>
        <div id="body"></div>
        <div id="articles-block"><?php echo articlesTitleHtml(
                loadLastArticles($config['count_articles'], $config)
            ); ?></div>
        <script type="text/javascript">
            var code = "<?php echo $config['code']; ?>"
            var mode = 0
        </script>
        <script type="text/javascript" src="main.js"></script>
        <noscript>Enable javascript execution!</noscript>
    </body>
</html>
