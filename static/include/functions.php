<?php

/**
 * @param array $data [key => string]
 * @param array $config [key => value]
 * @return array [string]
 */
function verification($data, $config)
{
    $errors = [];

    $errors = addError($errors,
        validRequired($data['name'], 'Обязательно укажите имя.')
    );
    $errors = addError($errors,
        validMin($data['name'], 3, 'Имя не короче трех символов.')
    );
    $errors = addError($errors,
        validMax($data['name'], 64, 'Имя не более 64 символов.')
    );
    $errors = addError($errors,
        validEmail($data['email'], 'Укажите корректный E-mail.')
    );
    $errors = addError($errors,
        validRequired($data['message'], 'Наберите ваше сообщение.')
    );
    $errors = addError($errors,
        validMax($data['message'], 64000, 'Очень длинное сообщение.')
    );
    $errors = addError($errors,
        validCode($data['code'], $config['code'], 'Спам не пройдет.')
    );

    return $errors;
}

/**
 * @param array $data [key => string]
 * @param array $config [key => value]
 * @return array [string]
 */
function sendEmail($data, $config)
{
    include(__DIR__.'/Email.php');

    $email = new \MMV\Functions\Email();
    $email->to($config['email'])
        ->from($config['email'], 'mmv-module.ru')
        ->subject('Message from mmv-module.ru')
        ->body("Time:\n=======\n".date('Y-m-d H:i:s', time())."\n\nName:\n=======\n{$data['name']}\n\nE-mail:\n=======\n{$data['email']}\n\nMessage:\n=======\n{$data['message']}\n\n");

    $message = '';

    try {
        $result = $email->send();
        if(!$result) $message = "Ошибка при отправки E-mail.";
    } catch (\Exception $e) {
        $message = $e->getMessage();
    }

    if($message) return [$message];
    else return [];
}

/**
 * @param int $n
 * @param array $config [key => value]
 * @return array [id, title, date, intro, file]
 */
function loadLastArticles($n, $config)
{
    $articles = include($config['path_articles'].'/config.php');
    return array_reverse(array_slice($articles, $n * -1));
}

/**
 * @param array $list [id, title, date, intro, file]
 * @return string
 */
function articlesTitleHtml($list)
{
    $result = [];

    foreach($list as $i) {
        $result[] = '<a href="'.urlArticle($i['id']).'">'.$i['title'].'</a>';
    }

    return '<p>'.implode('</p><p>', $result).'</p>';
}

/**
 * @param int $id
 * @param array $articles
 * @return array|false
 */
function findArticleById($id, &$articles)
{
    foreach($articles as $item) {
        if($item['id'] == $id) {
            return $item;
        }
    }

    return false;
}

/**
 * @param array $links [string]
 */
function loadHeadHtml($links=[])
{
    $result = file_get_contents(__DIR__.'/head.html');

    foreach($links as $item) {
        $result .= "\n        ".$item;
    }

    $result .= "\n    </head>\n";

    return $result;
}

/**
 * @param string $title
 * @param string $url
 * @return string
 */
function titleHtml($title, $url='')
{
    if($url) $title = '<a href="'.$url.'">'.$title.'</a>';

    return '<div class="title_page">'.$title.'</div>';
}

/**
 * @param array $article
 * @param array $config
 * @return string
 */
function loadArticleByName($article, $config)
{
    $text = dateHtml($article['date']);

    $text .= $article['intro']."\n<!--break-->\n\n";

    $text .= safeCodeView(
        file_get_contents($config['path_articles'].'/'.$article['file'])
    );

    return '<div class="post single">'.$text.'</div>';
}

/**
 * @param string $code
 * @return string
 */
function safeCodeView($code)
{
    $pattern = '/<!--precode(?:\|(.*))?-->(.*)<!--\/precode-->/sU';

    return preg_replace_callback(
        $pattern,
        function($matches){
            if($matches[1]) $class = ' class="'.$matches[1].'"';
            else $class = '';
            return "<!--precode-->\n<pre><code$class>"
                .str_replace('<', '&lt;', trim($matches[2]))
                ."</code></pre>\n<!--/precode-->";
        },
        $code
    );
}

/**
 * @param array $article
 * @return string
 */
function articlesHtml($articles)
{
    $result = [];

    foreach($articles as $item) {
        $result[] = titleHtml($item['title'], urlArticle($item['id']))
            .dateHtml($item['date'])
            .introHtml($item['intro'])
            .'<div class="more"><a href="'.urlArticle($item['id'])
                .'">читать ...</a></div>';
    }

    return '<div class="post">'
        .implode('</div><div class="post">', $result)
        .'</div>';
}

/**
 * @param string $date
 * @retur string
 */
function introHtml($text)
{
    return '<div class="intro">'.$text.'</div>';
}

/**
 * @param string $date
 * @retur string
 */
function dateHtml($date)
{
    return '<div class="date">'.$date.'</div>';
}

/**
 * @param int $id
 * @return string
 */
function urlArticle($id)
{
    return 'articles.php?id='.$id;
}

//////////////////////////////////////////////////

/**
 * @param array [string]
 * @param string $message
 * @return array [string]
 */
function addError($arr, $message)
{
    if($message) $arr[] = $message;

    return $arr;
}

/**
 * @param string $val
 * @param string $correct
 * @param string $message
 * @return string
 */
function validCode($val, $correct, $message)
{
    $result = '';
    if($val !== $correct) $result = $message;
    return $result;
}

/**
 * @param string $val
 * @param int $long
 * @param string $message
 * @return string
 */
function validMax($val, $long, $message)
{
    $result = '';
    if(mb_strlen($val) > $long) $result = $message;
    return $result;
}

/**
 * @param string $val
 * @param string $message
 * @return string
 */
function validRequired($val, $message)
{
    if($val) return '';
    else return $message;
}

/**
 * @param string $val
 * @param string $message
 * @return string
 */
function validEmail($val, $message)
{
    $pattern='/^[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/';
    if(preg_match($pattern, $val)) return '';
    else return $message;
}

/**
 * @param string $val
 * @param int $min
 * @param string $message
 * @return string
 */
function validMin($val, $min, $message)
{
    $result = '';
    if(mb_strlen($val) < $min) $result = $message;
    return $result;
}
