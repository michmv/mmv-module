///<reference path="../node_modules/@types/jquery/index.d.ts" />

import "./Import.d.ts"
import * as NNGI from "./NNGI"
import * as T from "./Types"
import * as Maket from "./Maket/Maket"
import * as E from "./Events"

const style = require("./style.sass")

let widget: NNGI.Main<T.Application> = new NNGI.Main()
widget.children.push(Maket.Factory())

let app: T.Application =
    { windowWidth: document.documentElement.clientWidth
    , windowHeight: document.documentElement.clientHeight
    , maxWidth: 960
    , minWidth: 320
    , fontSize: 14
    , code: code
    , mode: mode
    }

$(document).ready(function() {
    widget.build(app, undefined, $('#body').empty())

    $(window).resize( () => {
        app.windowWidth = document.documentElement.clientWidth
        app.windowHeight = document.documentElement.clientHeight
        widget.eventWidgetUp(new E.ReSize)
    })
});
