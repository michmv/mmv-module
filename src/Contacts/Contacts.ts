import { Page } from "../Page"
import * as T from "../Types"
import * as E from "../Events"

const style = require("./Contacts.sass")

export class Contacts extends Page
{
    menu: T.Menu = {
        name: "contacts",
        menu: "Контакты",
        page: "Обратная связь",
        offset: 0
    }
    form: JQuery<HTMLElement>
    dataString: string = ''
    ajaxStatus: boolean = false

    sendMessage()
    {
        if(this.ajaxStatus) { return }

        let dataString: string = this.form.serialize() + '&code=' +
            this.state.code

        if(dataString == this.dataString) {
            this._showErrors(['Повторный запрос.'])
        } else {
            this.ajaxStatus = true
            setTimeout((() => { this.lockForm() }).bind(this), 200)

            $.ajax({
                url: 'message.php',
                type: 'POST',
                dataType: 'json',
                data: dataString,
                success: ((data) => { this.success(data) }).bind(this),
                error: (
                    (jqXHR, textStatus, errorThrown) => {
                        this.error(jqXHR, textStatus, errorThrown)
                    }
                ).bind(this),
            });

            this.dataString = dataString
        }
    }

    success(data: T.Response)
    {
        if(data.status == 0) {
            this._showMessages(data.message)
            this._cleanForm()
        } else {
            this._showErrors(data.message)
        }

        this.unlockForm()
    }

    error(jqXHR: JQueryXHR, textStatus: string, errorThrown: string)
    {
        this._showErrors([
            'Ошибка: ' + jqXHR.status + ', ' + errorThrown
        ])

        this.unlockForm()
    }

    lockForm()
    {
        if(this.ajaxStatus) {
            this.form.addClass('lock')
            $('input', this.form).prop('disabled', true)
            $('textarea', this.form).prop('disabled', true)
        }
    }

    unlockForm()
    {
        this.form.removeClass('lock')
        $('input', this.form).prop('disabled', false)
        $('textarea', this.form).prop('disabled', false)

        this.ajaxStatus = false
    }

    protected _cleanForm()
    {
        $('input', this.form).val('')
        $('textarea', this.form).val('')
    }

    protected _showMessages(messages: Array<string>)
    {
        $('.response', this.form).empty().addClass('ok').removeClass('error')
            .append(this._createHtmlMessages(messages))
    }

    protected _showErrors(messages: Array<string>)
    {
        $('.response', this.form).empty().addClass('error').removeClass('ok')
            .append(this._createHtmlMessages(messages))
    }

    protected _createHtmlMessages(messages: Array<string>)
    {
        let result = []

        for(let i=0; i<messages.length; i++) {
            result.push($('<p>', {text: messages[i]}))
        }

        return result
    }

    protected _createHtml()
    {
        let element = [
            this._createHtmlForm(),
            this._createHtmlText(),
        ]

        return element
    }

    protected _createHtmlForm()
    {
        this.form = $('<form>', {class: "form", append: [
            $('<div>', {class: "response"}),
            $('<div>', {class: "box box1", append: [
                $('<textarea>', {
                    name: "message",
                    placeholder: "Сообщение",
                    fontSize: this.state.fontSize + 'px',
                    required: true
                }),
            ]}),
            $('<div>', {class: "box box2", append: [
                $('<input>',{name: "name", placeholder: "Имя", required: true}),
                $('<input>', {
                    name: "email",
                    type: "email",
                    placeholder: "E-mail",
                    required: true
                }),
                $('<div>', {
                    class: "send",
                    append: $('<button>', {
                        text: "Отправить",
                        append: $('<div>', {class: 'timer'})
                    })
                })
            ]})
        ]})

        this.form.submit((() => {
            this.sendMessage()
            return false
        }).bind(this))

        return this.form
    }

    protected _createHtmlText()
    {
        let email = '<div class="email"><p>mail</p>@mm<p>v-mo' +
            '</p><p>du</p>l<p>e.</p>ru</div>'

        return $('<br/><div>Или воспользуйтесь электронной почтой ' +
            email + '</div>')
    }

    protected _resize(event: E.ReSize)
    {
        this._setSizeForm(event.width_wrap)
    }

    protected _setSizeForm(width: number)
    {
        let formMode: T.Form = T.Form.Max
        let button = $('.send button', this.form)

        if(width < 700) formMode = T.Form.Def
        if(width < 440) formMode = T.Form.Min

        if(formMode == T.Form.Max) {
            this._setFormClass('max')
            $('input', this.form).removeAttr('style')

            $('textarea', this.form).height(130)
        } else if(formMode == T.Form.Min) {
            this._setFormClass('min')
            $('input', this.form).removeAttr('style')

            $('textarea', this.form).height(70)
        } else {
            this._setFormClass('def')

            let width_send = Math.ceil($('.send', this.form).width()) + 1

            $('input', this.form).css({
                width: (width - width_send) / 2 - this.state.fontSize + 'px',
                marginRight: Math.floor(this.state.fontSize) + 'px'
            })

            $('textarea', this.form).height(80)
        }

        $('.timer', button).width(button.outerWidth()).height(button.outerHeight())
    }

    protected _setFormClass(mode: string)
    {
        this.form.removeClass(['max', 'min', 'def']).addClass(mode)
    }
}
