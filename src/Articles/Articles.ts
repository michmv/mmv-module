import { Page } from "../Page"
import * as T from "../Types"

const style = require("./Articles.sass")

export class Articles extends Page
{
    menu: T.Menu = {
        name: "articles",
        menu: "Статьи",
        page: "Некоторые статьи",
        offset: 0
    }

    protected _createHtml()
    {
        let element = $('#articles-block')

        return [element]
    }
}
