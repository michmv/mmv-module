import { Page } from "../Page"
import * as T from "../Types"
import * as NNGI from "../NNGI"
import * as E from "../Events"

const style = require("./Articles.sass")

export class ArticleItem extends Page
{
    menu: T.Menu = {
        name: "articles",
        menu: "Блог",
        page: "Заголовок",
        offset: 0
    }
    mode = 1

    draw(target: JQuery<HTMLElement>)
    {
        let element: JQuery<HTMLElement>

        if(this.mode == this.state.mode) {
            element = $('<div>', {id: this.menu.name, class: "module"})
                .append($('<div>', {
                    class: 'wrap',
                }).append(this._createHtml())
                )

            target.append(element)
        } else {
            element = target
        }

        return {it: element, container: target}
    }

    afterDraw()
    {
        $('pre code').each((index, block) => {
            hljs.highlightBlock(block)
        })

        $('.lightbox').on('click', (e) => {
            (<any>$).magnificPopup.open({
                items: {
                    src: e.target.getAttribute('href')
                },
                type: 'image',
            })
            return false
        })
    }

    event(event: NNGI.EventWidget): NNGI.EventWidget
    {
        if(!(event instanceof E.ListMenu)) {
            event = super.event(event)
        }

        return event
    }

    protected _createHtml()
    {
        let element = $('#articles-block')

        return [element]
    }
}
