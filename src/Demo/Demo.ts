import { Page } from "../Page"
import * as T from "../Types"
import * as E from "../Events"

const style = require("./Demo.sass")

export class Demo extends Page
{
    menu: T.Menu = {
        name: "demo",
        menu: "Демо",
        page: "Демки",
        offset: 0
    }

    protected _load: Array<T.Demo> =
        [ {title: "Sokoban", description: "Тест WebGL.", url: "demos/sokoban"}
        , {title: "2048", description: "Клон игры 2048.", url: "demos/e2048"}
        , {title: "Музыкальная грамотность", description: "Изучение музыкальных нот.", url: "demos/mliteracy"}
        , {title: "Пятнашка", description: "Версия игры пятнадцать.", url: "demos/game15"}
        ]

    protected _createHtml(): Array<JQuery<HTMLElement>>
    {
        let element = $('<div>', {class: 'demos'})

        for(let i=0; i<this._load.length; i++) {
            element.append(this._createHtmlItem(this._load[i]))
        }

        return [element]
    }

    protected _createHtmlItem(item: T.Demo): JQuery<HTMLElement>
    {
        let e = $('<div>', {class: 'item', append:
            $('<a>', {href: item.url, append:[
                $('<div>', {class: 'title', text: item.title}),
                $('<div>', {
                    class: "description",
                    text: item.description
                })
            ]})
        })

        return e
    }

    protected _resize(event: E.ReSize)
    {
        let width_work = event.width_wrap - this.state.minWidth
        let max_width = this.state.maxWidth - this.state.minWidth
        let n = Math.ceil(width_work / (max_width / 3))
        if(n == 0) n = 1

        let width_item = (event.width_wrap - 1 - (n - 1) * 20) / n

        $('.item', this.it).width(width_item)
    }
}
