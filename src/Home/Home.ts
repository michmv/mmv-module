import { Page } from "../Page"
import * as T from "../Types"
import * as E from "../Events"
import * as NNGI from "../NNGI"

const style = require("./Home.sass")

export class Home extends Page
{
    menu: T.Menu = {
        name: "home",
        menu: "Главная",
        page: "Разработка веб-приложений",
        offset: 0
    }
    widthImage: number = 800
    heightImage: number = 421

    event(event: NNGI.EventWidget): NNGI.EventWidget
    {
        if(!(event instanceof E.ListMenu)) {
            event = super.event(event)
        }

        return event
    }

    protected _createHtml()
    {
        let element = [
            this._createHtmlElevator(),
        ]

        return element
    }

    protected _createHtmlElevator()
    {
        let element = $('<div>', {
            class: "elevator",
            append: [
                $('<img>', {src: "assets/elevator.jpg"}),
                this._createHtmlLanguage()
            ]
        })

        return element
    }

    protected _createHtmlLanguage()
    {
        let element = $('<div>', {class: "text"})
            .append($('<div>', {class: "languages", append: [
                $('<div>', {class: "item"})
                    .append($('<div>', {
                        class: "lang",
                        text: "JavaScript",
                        append: [
                            $('<div>', {class: "lang", text: "Html5"}),
                            $('<div>', {class: "lang", text: "CSS3"})
                        ]
                    })),
                $('<div>', {class: "item"})
                    .append($('<div>', {
                        class: "lang",
                        text: "Php",
                        append: [
                            $('<div>', {class: "lang", text: "MySql"})
                        ]
                    }))
            ]}))

        return element
    }

    protected _resize(event: E.ReSize)
    {
        this._setSizeElevator(event.width_wrap)
        this._setSizeLanguages(event.width_wrap)
    }

    protected _setSizeLanguages(width: number)
    {
        let height = width * 0.028
        if(height < this.minHeightTitle - 1) height = this.minHeightTitle - 1

        $('.languages', this.it).css({
            fontSize: height + 'px'
        })
    }

    protected _setSizeElevator(width: number)
    {
        let heightElevator = width / (this.widthImage / this.heightImage)
        if(heightElevator > this.heightImage) heightElevator = this.heightImage

        let offsetX = 0
        let imageW = width
        let imageH = heightElevator
        let marginTop = 0
        let maxWidthForMagin = 540

        if(width <= maxWidthForMagin) {
            let x1 = this.state.minWidth, y1 = 40
            let x2 = maxWidthForMagin,    y2 = 0
            let a = (y2 - y1) / (x2 - x1)
            let b = y1 - x1 * a
            marginTop = width * a + b
        }

        if(width > this.widthImage) {
            imageW = this.widthImage
            offsetX = width - imageW
            imageH = this.heightImage
        }

        let elevator = $('.elevator', this.it)
            .height(heightElevator + marginTop)
        $('img', elevator).css({
            width: imageW + 'px',
            height: imageH + 'px',
            left: offsetX + 'px',
            top: marginTop + 'px'
        })
    }
}
