import * as NNGI from "../NNGI"
import * as T from "../Types"
import * as Home from "../Home/Home"
import * as Portfolio from "../Portfolio/Portfolio"
import * as Contacts from "../Contacts/Contacts"
import * as Demo from "../Demo/Demo"
import * as Articles from "../Articles/Articles"
import * as ArticleItem from "../Articles/ArticleItem"
import * as E from "../Events"

const style = require("./Maket.sass")

export function Factory<T>(): Maket
{
    let maket: Maket = new Maket

    maket.children.push(new Home.Home)
    maket.children.push(new Demo.Demo)
    maket.children.push(new Portfolio.Portfolio)
    maket.children.push(new Contacts.Contacts)
    maket.children.push(new Articles.Articles)
    maket.children.push(new ArticleItem.ArticleItem)

    return maket
}

export class Maket extends NNGI.Widget<T.Application>
{
    header: JQuery<HTMLElement>
    footer: JQuery<HTMLElement>
    buttonUp: JQuery<HTMLElement>

    heightHeader:       number = 85
    marginTopFooter:    number = 20
    heightFooter:       number = 2
    marginBottomFooter: number = 31

    menuMode: T.MenuMode = T.MenuMode.Horizontal
    menu: Array<T.Menu> = []

    draw(target: JQuery<HTMLElement>)
    {
        let container = $('<div>', {
            id: "content",
            minWidth: this.state.minWidth + 'px'
        })

        this.buttonUp = $('<div>', {class: "up", append: [
                $('<canvas>',
                    {id: "up-button"}).attr({width: 200, height: 35}),
                $('<div>', {class: "text", text: "наверх"})
            ]}),

        this.footer = $('<div>', {
            id: "footer",
            append: [
                $('<div>', {class: "year",text: "2010 - " + this._getYear()}),
                this.buttonUp
            ]
        })
        let main = $('<div>', {id: "main"}).append([container, this.footer])
        $(document).on('click', (() => { this.clickGlobal() }).bind(this))
        target.css('font-size', this.state.fontSize + 'px')

        target.append(main)

        return {it: main, container: container}
    }

    afterDraw()
    {
        // up button
        this.drawButtonUp()
        this.buttonUp.on('click',(() => {
                this.scrollPage(0)
            }).bind(this))
        $(document).on('scroll', (() => {
                this.showUpButton()
            }).bind(this))

        let list = <E.ListMenu>(this.eventWidgetUp(new E.ListMenu))
        this._addHeaderHtml(list.menu)

        $('.module', this.it).each((i, obj) => {
            if(i % 2 > 0) {
                $(obj).addClass('even')
            }
        })

        this.state.windowWidth = document.documentElement.clientWidth
        this.state.windowHeight = document.documentElement.clientHeight
        this.eventWidgetUp(new E.ReSize)

        // scroll after load page
        let mark = window.location.hash.replace("#","")
        if(mark) {
            let event = this.eventWidgetUp(new E.ScrollPosition(mark))
            this.scrollPage((<E.ScrollPosition>event).position)
        }
    }

    event(event: NNGI.EventWidget): NNGI.EventWidget
    {
        if(event instanceof E.ReSize) {
            event = this._size(event)
        }

        return event
    }

    showUpButton()
    {
        let y = $(document).scrollTop()
        if(y > this.heightHeader) {
            this.buttonUp.addClass('show')
        } else {
            this.buttonUp.removeClass('show')
        }
    }

    showMenu()
    {
        $('.menu', this.header).css('top', this.heightHeader + 'px')
    }

    hideMenu()
    {
        $('.menu', this.header).css('top', '-3000px')
    }

    clickGlobal()
    {
        if(this.menuMode == T.MenuMode.Vertical) {
            this.hideMenu()
        }
    }

    clickMenu(name: string)
    {
        if(this.state.mode == 0) {
            let event = this.eventWidgetUp(new E.ScrollPosition(name))
            this.scrollPage((<E.ScrollPosition>event).position)

            return false
        } else {
            return true
        }
    }

    drawButtonUp()
    {
        let canvas = document.getElementById('up-button') as HTMLCanvasElement
        let context = canvas.getContext('2d');

        let startX = 2
        let maxY = 35
        let minY = 2
        let offsetX = 10
        let radius = 5
        let finishX = 198

        context.beginPath()
        context.moveTo(startX, maxY)
        context.lineTo(startX + offsetX, minY + radius) // 1
        context.arc(startX + offsetX + radius, minY + radius, radius, Math.PI, Math.PI*1.5) // 2
        context.lineTo(finishX - offsetX - radius, minY) // 3
        context.arc(finishX - offsetX - radius, minY + radius, radius, Math.PI*1.5, Math.PI*2) // 4
        context.lineTo(finishX, maxY) // 5
        context.lineWidth = 2
        context.stroke()
        context.fillStyle = 'rgba(255, 255, 255, 0.75)'
        context.fill()
        context.closePath()
    }

    scrollPage(n: number)
    {
        $("body,html").stop().animate({"scrollTop": n}, 400)
    }

    protected _addHeaderHtml(list: Array<T.Menu>)
    {
        let menu = $('<div>', {class: "menu"})
        let item: JQuery<HTMLElement>

        for(let i=0; i<list.length; i++) {
            item = $('<a>', {
                class: "item",
                text: list[i].menu,
                "data-name": list[i].name,
                href: '.#' + list[i].name,
                click: (() => {
                    this.clickMenu(list[i].name)
                }).bind(this)
            })
            menu.append($('<div>', {
                class: "item_box",
                append: item
            }))
        }

        this.header = $('<div>', {id: "header"})
        this.header.append([
            $('<a>', {
                href: '.',
                append: $('<img>', {class: "logo", src: "assets/logo.png"})
            }),
            menu,
            $('<div>', {class: "menu_button"}).on('click', (() => {
                this.showMenu()
                return false
            }).bind(this))
        ])

        this.it.append(this.header)
    }

    protected _size(event: E.ReSize)
    {
        let width_window = this.state.windowWidth
        if(this.state.windowWidth < this.state.minWidth) {
            width_window = this.state.minWidth
        }

        let width_work = width_window
        if(this.state.windowWidth > this.state.maxWidth)
            width_work = this.state.maxWidth

        // event parameters for children
        event.width_container = width_window
        event.width_wrap = width_work - 30
        event.margin_left = (width_window - width_work) / 2 + 15

        this.container.css({
            paddingTop: this.heightHeader + 'px',
        })

        this.footer.css({
            width: width_work - 30 + 'px',
            height: this.heightFooter + 'px',
            marginLeft: (width_window - width_work) / 2 + 15 + 'px',
            marginTop: this.marginTopFooter + 'px',
            marginBottom: this.marginBottomFooter + 'px'
        })

        if(this.header !== undefined) {
            this.header.css({
                height: this.heightHeader + 'px',
                minWidth: this.state.minWidth + 'px'
            })

            $('.logo', this.header).css({
                top: 10 + 'px',
                left: (width_window - width_work) / 2 + 15 + 'px'
            })
        }

        this._sizeMenu(width_window, width_work)

        return event
    }

    protected _sizeUpButton(width: number)
    {
        this.buttonUp.css({
            left: (width - 200) / 2 + 'px'
        })
    }

    protected _sizeMenu(width_window: number, width_work: number)
    {
        let width_menu = width_work - 220 - 30
        let total_width_item = 0
        $('.menu .item', this.header).each((index, value) => {
            total_width_item = total_width_item + $(value).outerWidth()
        })

        if(width_menu > total_width_item) {
            $('.menu_button', this.header).css('display', 'none')
            $('.menu', this.header).css({
                top: (this.heightHeader - 35) / 2 + 0 + 'px',
                left: (width_window - width_work) / 2 + 15 + 220 + 'px',
                width: width_work - 15 - 220 - 15 + 'px'
            }).removeClass('vertical').addClass('horizontal')

            this.menuMode = T.MenuMode.Horizontal
        } else {
            $('.menu_button', this.header).css({
                display: "block",
                left: width_work - 15 - 24 + 'px'
            })
            $('.menu', this.header).css({
                // top: this.heightHeader + 'px',
                left: 'auto',
                width: 'auto'
            }).removeClass('horizontal').addClass('vertical')

            if(this.menuMode == T.MenuMode.Horizontal) {
                $('.menu', this.header).css('top', '-3000px')
            }

            this.menuMode = T.MenuMode.Vertical
        }

        this._sizeUpButton(width_window)
    }

    protected _getYear(): number
    {
        let d = new Date();
        return d.getFullYear();
    }
}
