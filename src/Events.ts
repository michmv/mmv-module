import * as NNGI from "./NNGI"
import * as T from "./Types"

/*
export class LoadPage extends NNGI.EventWidget
{
    constructor(public id: string)
    {
        super()
    }
}
*/

export class ReSize extends NNGI.EventWidget
{
    width_container: number = 0
    width_wrap: number = 0
    margin_left: number = 0
}

export class ListMenu extends NNGI.EventWidget
{
    menu: Array<T.Menu> = []
}

export class ScrollPosition extends NNGI.EventWidget
{
    position: number = 0

    constructor(public name: string) { super() }
}
