import * as NNGI from "./NNGI"
import * as T from "./Types"
import * as E from "./Events"
import * as Maket from "./Maket/Maket"

const style = require("./Page.sass")

export abstract class Page extends NNGI.Widget<T.Application>
{
    menu: T.Menu = {name: "page", menu: "Страница", page: "Заголовок страницы", offset: 0}
    mode: number = 0
    minHeightTitle: number = 19

    draw(target: JQuery<HTMLElement>)
    {
        let element: JQuery<HTMLElement>

        if(this.mode == this.state.mode) {
            element = $('<div>', {id: this.menu.name, class: "module"})
                .append($('<div>', {
                    class: 'wrap',
                    append: $('<div>', {class: 'title_page', text: this.menu.page})
                }).append(this._createHtml())
                )

            target.append(element)
        } else {
            element = target
        }

        return {it: element, container: target}
    }

    event(event: NNGI.EventWidget): NNGI.EventWidget
    {
        if(event instanceof E.ListMenu) {
            event.menu.push(this.menu)
        }

        if(event instanceof E.ReSize) {
            this._resizeWrap(event)
            if(this.mode == this.state.mode) {
                this._resize(event)
            }

            this.menu.offset = this.it.offset().top
        }

        if(event instanceof E.ScrollPosition) {
            if(event.name == this.menu.name) {
                event.status = NNGI.Status.Stop
                event.position = this.menu.offset
            }
        }

        return event
    }

    protected _resizeWrap(event: E.ReSize)
    {
        $('.wrap', this.it).css({
            width: event.width_wrap,
            marginLeft: event.margin_left
        })

        this.it.css('min-width', this.state.minWidth + 'px')

        this._setSizeTitle(event.width_wrap)
    }

    protected _resize(event: E.ReSize) {}

    protected _createHtml(): Array<JQuery<HTMLElement>>
    {
        return [$('<div>', {text: this.menu.name})]
    }

    protected _setSizeTitle(width: number)
    {
        let height = width * 0.031
        if(height < this.minHeightTitle) height = this.minHeightTitle

        $('.title_page', this.it).css({
            fontSize: height + 'px',
            lineHeight: height + 'px'
        })
    }
}
