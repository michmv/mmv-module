export interface Application
    { windowWidth: number
    , windowHeight: number
    , maxWidth: number
    , minWidth: number
    , fontSize: number
    , code: string
    , mode: number // 0 - site, 1 - block 
    }

export interface Menu
    { name: string
    , page: string
    , menu: string
    , offset: number
    }

export interface Item
    { object: JQuery<HTMLElement>
    , width: number
    }

export interface Response
    { status: number
    , message: Array<string>
    }

export interface Project
    { title: string
    , preview: string
    , image: string
    }

export interface Gallery
    { src: string
    , title: string
    }

export interface Demo
    { title: string
    , description: string
    , url: string
    }

export enum MenuMode {Horizontal, Vertical}

export enum Form {Max, Min, Def}

/*
run scroll
stop scroll
click image

run scroll
    set time

click image
    if diff time ~ 0 click image

*/
