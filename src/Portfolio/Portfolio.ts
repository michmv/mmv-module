import { Page } from "../Page"
import * as T from "../Types"
import * as E from "../Events"

const style = require("./Portfolio.sass")

export class Portfolio extends Page
{
    menu: T.Menu = {
        name: "portfolio",
        menu: "Портфолио",
        page: "Некоторые проекты",
        offset: 0
    }

    widthItem: number = 230
    heightItem: number = 250

    scroll: boolean = false // scroll mode
    selectText: boolean = false // select text mode
    firstClickPositionX: number = 0
    scrollPositionX: number = 0
    clickTime: Date = new Date

    protected _load: Array<T.Project> =
        [ {image: "assets/files/portfolio/8_10/kochergi.jpg", preview: "assets/files/portfolio/8_10/kochergi_small.jpg", title: "kochergi.ru"}
        , {image: "assets/files/portfolio/8_10/shortway.jpg", preview: "assets/files/portfolio/8_10/shortway_small.jpg", title: "shortway.ru"}
        , {image: "assets/files/portfolio/8_10/sm-klimat.jpg", preview: "assets/files/portfolio/8_10/sm-klimat_small.jpg", title: "sm-klimat.ru"}
        , {image: "assets/files/portfolio/8_10/stonemagazine.jpg", preview: "assets/files/portfolio/8_10/stonemagazine_small.jpg", title: "stonemagazine.ru"}
        , {image: "assets/files/portfolio/8_10/zheleznysad.jpg", preview: "assets/files/portfolio/8_10/zheleznysad_small.jpg", title: "zheleznysad.ru"}
        , {image: "assets/files/portfolio/blueribbon.jpg", preview: "assets/files/portfolio/small_blueribbon.jpg", title: "blueribbon.ru"}
        , {image: "assets/files/portfolio/virtual-petersburg.jpg", preview: "assets/files/portfolio/small_virtual-petersburg.jpg", title: "virtual-petersburg.ru"}
        , {image: "assets/files/portfolio/nadejda_site_01.jpg", preview: "assets/files/portfolio/small_nadejda_site_01.jpg", title: "nadegdaplus.ru"}
        , {image: "assets/files/portfolio/virtualpetersburg.jpg", preview: "assets/files/portfolio/small_virtualpetersburg.jpg", title: "virtualpetersburg.ru"}
        , {image: "assets/files/portfolio/ilovepetersburg.jpg", preview: "assets/files/portfolio/small_ilovepetersburg.jpg", title: "ilovepetersburg.ru"}
        , {image: "assets/files/portfolio/tvsz_site_01.jpg", preview: "assets/files/portfolio/small_tvsz_site_01.jpg", title: "www.tvsz.ru"}
        , {image: "assets/files/portfolio/worldec_s.jpg", preview: "assets/files/portfolio/small_worldec_s.jpg", title: "worldec.ru"}
        , {image: "assets/files/portfolio/zbd_1.jpg", preview: "assets/files/portfolio/small_zbd_1.jpg", title: "chf-fund.spb.ru"}
        , {image: "assets/files/portfolio/kicx_1.jpg", preview: "assets/files/portfolio/small_kicx_1.jpg", title: "kicx.ru"}
        , {image: "assets/files/portfolio/agral_s.jpg", preview: "assets/files/portfolio/small_agral_s.jpg", title: "agral.ru"}
        , {image: "assets/files/portfolio/3v_solutions_mainpage1.jpg", preview: "assets/files/portfolio/small_3v_solutions_mainpage1.jpg", title: "3v-solutions.ru"}
        , {image: "assets/files/portfolio/stincom_site_1.jpg", preview: "assets/files/portfolio/small_stincom_site_1.jpg", title: "stin-com.ru"}
        , {image: "assets/files/portfolio/parabola_index.jpg", preview: "assets/files/portfolio/small_parabola_index.jpg", title: "parabola-group.ru"}
        , {image: "assets/files/portfolio/absolute_future_site_01.jpg", preview: "assets/files/portfolio/small_absolute_future_site_01.jpg", title: "absolut-future.ru"}
        , {image: "assets/files/portfolio/viewpetersburg.jpg", preview: "assets/files/portfolio/small_viewpetersburg.jpg", title: "virtualpetersburg.ru"}
        , {image: "assets/files/portfolio/geizer_1.jpg", preview: "assets/files/portfolio/small_geizer_1.jpg", title: "geizer.com"}
        , {image: "assets/files/portfolio/ictgroup_site.jpg", preview: "assets/files/portfolio/small_ictgroup_site.jpg", title: "ict-group.ru"}
        , {image: "assets/files/portfolio/tkdom_main_page.jpg", preview: "assets/files/portfolio/small_tkdom_main_page.jpg", title: "tkdom.ru"}
        , {image: "assets/files/portfolio/olmeca_gf_site.jpg", preview: "assets/files/portfolio/small_olmeca_gf_site.jpg", title: "olmeca.ru"}
        , {image: "assets/files/portfolio/audiomusic_site_01.jpg", preview: "assets/files/portfolio/small_audiomusic_site_01.jpg", title: "audiomusic-hifi.ru"}
        , {image: "assets/files/portfolio/tkdom_interior_site_01.jpg", preview: "assets/files/portfolio/small_tkdom_interior_site_01.jpg", title: "tkdom-interior.ru"}
        , {image: "assets/files/portfolio/parket_trade_1.jpg", preview: "assets/files/portfolio/small_parket_trade_1.jpg", title: "parket-trade.ru"}
        , {image: "assets/files/portfolio/tor_service_mainpage.jpg", preview: "assets/files/portfolio/small_tor_service_mainpage.jpg", title: "tor-service.ru"}
        ]

    protected _resize(event: E.ReSize)
    {
        this._setSizeGallery(event.width_wrap)
    }

    protected _setSizeGallery(width: number)
    {
        let height = this._getHeight()
        let countH = Math.floor(height / this.heightItem)
        if(countH < 1) countH = 1
        let countW = Math.ceil(this._load.length / countH)

        $('.gallery_box', this.it).width(countW * this.widthItem - 30)
            .height(countH * this.heightItem)

        $('.item', this.it).removeClass(['first', 'last'])
        let i = 0
        while(i < this._load.length) {
            $('.id'+i, this.it).addClass('first')
            $('.id'+(i+countW-1), this.it).addClass('last')
            i = i + countW
        }
    }

    protected _getHeight(): number
    {
        return document.documentElement.clientHeight -
            parseInt(this.it.css('padding-top')) -
            parseInt(this.it.css('padding-bottom')) -
            $('.title_page', this.it).outerHeight()
            - 15
    }

    protected _runScroll(
            e: any,
            element: JQuery<HTMLElement>,
            box: JQuery<HTMLElement>
        )
        {
        if(!this.selectText && e.button == 0) {
            this.clickTime = new Date
            this.scroll = true
            this.firstClickPositionX = e.pageX
            this.scrollPositionX = element.scrollLeft()
            // disable select
            box.css({
                "-moz-user-select":    "none",
                "-khtml-user-select":  "none",
                "-webkit-user-select": "none",
                "-o-user-select":      "none",
                "user-select":         "none",
                // "cursor":              "move"
            })
            return false
        }
    }

    protected _stopScroll(box: JQuery<HTMLElement>)
    {
        if(!this.selectText && this.scroll) {
            this.scroll = false
            // enable select
            box.css({
                "-moz-user-select":    "auto",
                "-khtml-user-select":  "auto",
                "-webkit-user-select": "auto",
                "-o-user-select":      "auto",
                "user-select":         "auto",
                // "cursor":              "auto"
            })
            return false
        }
    }

    protected _createHtml(): Array<JQuery<HTMLElement>>
    {
        let box = $('<div>', {class: "gallery_box"})
        let element = $('<div>', {class: "gallery", append: box})
        let list = this._load

        // scroll mode
        box.mousedown((function(e){
            return this._runScroll(e, element, box)
        }).bind(this)).mouseup((function(e){
            return this._stopScroll(box)
        }).bind(this))

        $(document).on('mousemove', (function(e){
            // scrool
            if(this.scroll) {
                let offsetX = this.firstClickPositionX - e.pageX
                element.scrollLeft(this.scrollPositionX + offsetX)
            }
        }).bind(this)).mouseleave((function(){
            // scroll mode off
            return this._stopScroll(box)
        }).bind(this)).mouseup((function(){
            // scroll mode off
            return this._stopScroll(box)
        }).bind(this))

        // create item
        for(let i=0; i<list.length; i++) {
            box.append(this._createHtmlItem(list[i], i))
        }

        $('.text', box).mousedown((function(e){
            if(e.button == 0)
                this.selectText = true // select text on
        }).bind(this)).mouseup((function(e){
            if(e.button == 0)
                this.selectText = false // select text off
        }).bind(this))

        return [element]
    }

    protected _createHtmlItem(item: T.Project, n: number): JQuery<HTMLElement>
    {
        let link = $('<a>', {
            class: "image",
            href: item.image,
            append: $('<img>', {class: "preview", src: item.preview})
        })

        let element = $('<div>', {
            class: "item id" + n,
            append: [
                link,
                $('<div>', {
                    class: "text_box",
                    append: $('<div>', {class: "text", text: item.title})
                })
            ]
        })

        link.on('click', (() => {
            let end: any = new Date
            let diff: number = end - <any>this.clickTime
            if(diff <= 200) {
                this._showGallery(n)
                return false
            } else {
                return false
            }
        }).bind(this))

        return element
    }

    protected _showGallery(n: number)
    {
        let list: Array<T.Gallery> = []
        for(let i=0; i<this._load.length; i++) {
            list.push({src: this._load[i].image, title: this._load[i].title})
        }

        let t = (<any>$).magnificPopup.open({
            items: list,
            type: 'image',
            gallery:{
                enabled:true,
                index: n
            },
            index: n
        })

        let magnificPopup: any = (<any>$).magnificPopup.instance
        magnificPopup.goTo(n)
    }
}
