const path = require('path')
const CopyPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
    entry: [
        './src/Main.ts',
    ],
    output: {
        filename: './main.js',
        path: path.resolve(__dirname, "dist/public")
    },
    // devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                // use: ['cache-loader', 'ts-loader'],
                use: [
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: true,
                            experimentalWatchApi: true,
                        },
                    },
                ],
                exclude: /node_modules|\.d\.ts$/,
            },
            {
                test: /\.d\.ts$/,
                loader: 'ignore-loader'
            },
            {
                test: /\.s[ac]ss$/i,
                include: path.resolve(__dirname, 'src'),
                use: [
                    MiniCssExtractPlugin.loader,
                    'cache-loader',
                    {
                        loader: "css-loader",
                        options: {
                            url: false
                        }
                    },
                    'sass-loader',
                ],
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                loader: 'file-loader',
                options: {
                    name(file) {
                        let regexp = new RegExp(/^.*\/src\//, 'i')
                        return file.replace(regexp, 'assets/')
                    },
                },
            },
        ]
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js', '.sass', '.jpg', '.svg', '.png', '.gif'],
    },
    plugins: [
        // Copy folder
        new CopyPlugin([
                { from: 'static', to: '.' },
                { from: 'articles', to: '../articles' },
            ]),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css',
        }),
    ]
}